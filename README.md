# p2k8g2b2 (Final Project 2)

p2k8g2b2 adalah sebuah proyek akhir ke-2 oleh (Project 2) Kelompok 8, Grup 2, Batch 2 dari kelas Full Stack Web Development, PKS Digital School, 2022.

## (Project 2) Kelompok 8 (Grup 2, Batch 2)

- Dhia Aziz Rizqi Arrahman (@naruti_)
- Muhammad Alif (@muhammadalif1418)
- Puguh Kujatmiko
- Edy Santoso (@achedy)

## Tema (Skenario)

p2k8g2b2 adalah sebuah penerbit buku. Mereka membutuhkan sebuah aplikasi web dengan nama yang sama. *Web app* tersebut digunakan untuk menampung ulasan-ulasan *(reviews)* dari para pembaca yang terdaftar *(users)* untuk buku-buku khusus terbitannya *(books)*.

## ERD

![ERD p2k8g2b2](p2k8g2b2-erd.png)

## Tautan

- Demo: https://youtu.be/vSQX3U0vAh0
- Screenshot: https://drive.google.com/drive/folders/11LxN7WR-EZ6Qxc0LmP_iQwBYE8WlTLV1?usp=sharing
- Dokumentasi web service: https://documenter.getpostman.com/view/20065830/UVyyrsGV
- Backend: https://p2k8g2b2.herokuapp.com/api
- Frontend: https://p2k8g2b2.netlify.app/
