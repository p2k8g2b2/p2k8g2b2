export default {
  namespaced: true,
  state: {
    status: false,
    color: 'success',
    text: '',
  },
  getters: {
    status: state => state.status,
    color: state => state.color,
    text: state => state.text,
  },
  mutations: {
    set: (state, pay) => {
      state.status = pay.status
      state.color = pay.color
      state.text = pay.text
    },
  },
  actions: {
    set: ({commit}, pay) => {
      commit('set', pay)
    },
  },
}
