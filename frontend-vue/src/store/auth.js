import axios from 'axios'

export default {
  namespaced: true,
  state: {
    token: '',
    user: {},
  },
  getters: {
    user: state => state.user,
    token: state => state.token,
    // member: state => Object.keys(state.user).length !== 0,
    member: state => Object.keys(state.user).length,
  },
  mutations: {
    setToken: (state, pay) => {
      state.token = pay
    },
    setUser: (state, pay) => {
      state.user = pay
    },
  },
  actions: {
    setToken: ({commit, dispatch}, pay) => {
      commit('setToken', pay)
      dispatch('checkToken', pay)
    },
    checkToken: ({commit}, pay) => {
      const con = {
        method: 'post',
        url: 'https://p2k8g2b2.herokuapp.com/api/auth/me',
        // url: 'http://localhost:8000/api/auth/me',
        headers: {
          'Authorization': 'Bearer ' + pay,
        },
      }

      axios(con)
          .then(res => {
            commit('setUser', res.data)
          })
          .catch(() => {
            commit('setUser', {})
            commit('setToken', '')
          })
    },
    setUser: ({commit}, pay) => {
      commit('setUser', pay)
    },
  },
}
