import Vue from 'vue'
import Vuex from 'vuex'
import snackbar from './snackbar'
import modal from './modal'
import auth from './auth'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'p2k8g2b2',
  storage: localStorage,
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules: {
    snackbar,
    modal,
    auth,
  },
})
