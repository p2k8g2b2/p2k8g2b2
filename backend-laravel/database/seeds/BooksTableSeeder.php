<?php

use App\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::create([
            'title' => 'Nichijou, 1',
            'author' => 'Keiichi Arawi',
            'description' => 'In this just-surreal enough take on the “school genre” of manga, a group of friends grapple with all sorts of unexpected situations in their daily lives as high schoolers.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/wmlaz8.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 2',
            'author' => 'Keiichi Arawi',
            'description' => 'Yuuko tries a string of puns that goes very far off the rails. Misato pulls out the big guns when dealing with the maddeningly level-headed Sasahara. The professor adopts a surprisingly chatty cat, and a dog shows up at the right time to lend a paw.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/hk4gqi.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 3',
            'author' => 'Keiichi Arawi',
            'description' => 'The professor gets everyone stuck in a sticky situation. Mio goes to extraordinary lengths to protect her passionate pet project. Unexpected feelings for another teacher have taken root in Mr. Takasaki’s heart. And an unfortunate misunderstanding ends up bringing two friends closer together.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/u71ruq.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 4',
            'author' => 'Keiichi Arawi',
            'description' => 'The denizens of the Fey Kingdom must placate their princess. Mio invents an explosive athletic move. A newly-opened cafe bedevils Yuuko. Yuuko seizes a chance to send Mio’s already-active imagination into overdrive, and Nakamura, the science teacher, takes a professional interest in Nano.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/bx456i.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 5',
            'author' => 'Keiichi Arawi',
            'description' => 'Mio enlists her friends to help her meet a major manga deadline. Mr. Takasaki makes a deal with the devil to get closer to his crush. Sakamoto’s scarf is repurposed for a chatty crow. A sacred temple must fend off a new brand of fiend, and Ms. Nakamura is tripped up in her quest to capture Nano by a deeply unscientific reaction to compliments.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/jvoexf.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 6',
            'author' => 'Keiichi Arawi',
            'description' => 'Mio finally has her dream come true. Ms. Nakamura ends up in a nightmare situation after discovering the Shinonome Lab. The Princess goes on a rampage to recover wood cubes. Mai makes a new friend, and Nio makes a heartbreaking discovery that propels her to learn something important about life…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/gygoq9.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 7',
            'author' => 'Keiichi Arawi',
            'description' => 'Mio makes an abrupt confession. Mihoshi tries to help her sister Misato confess her feelings to her crush. Mai heeps pulling the rug on Yuuko with her deceptive sense of humor. Tanaka stubbornly refuses to break a promise to Nakanojou. Yuuko gets mistaken for a celebrity, and Mr. Takasaki unwittingly foils Ms. Nakamura’s sneaky plans…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/zsokg8.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 8',
            'author' => 'Keiichi Arawi',
            'description' => 'A U.F.O. causes Yuuko’s lunch to come out her nose. Ms. Nakamura tries an adorable new ploy. Mihoshi’s big plan backfires. Mai successfully dodges a trap. Hypnosis doesn’t work, until it works all too well. And Weboshi might actually be able to read minds…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/pjb2eg.jpg',
        ]);

        Book::create([
            'title' => 'Nichijou, 9',
            'author' => 'Keiichi Arawi',
            'description' => 'Yukko has a depressing doppelganger. Tanaka has a secret weapon. Mai becomes engrossed in a new best-seller. Mio squares off with a strange new vending machine, and a near-death experience creates chaos for a guardian angel…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/rp5s8d.jpg',
        ]);

        Book::create([
            'title' => 'Sculpting in Time',
            'author' => 'Andrei Tarkovsky',
            'description' => 'Andrei Tarkovsky, the genius of modern Russian cinema—hailed by Ingmar Bergman as “the most important director of our time”—died an exile in Paris in December 1986. In Sculpting in Time, he has left his artistic testament, a remarkable revelation of both his life and work. Since Ivan’s Childhood won the Golden Lion at the Venice Film Festival in 1962, the visionary quality and totally original and haunting imagery of Tarkovsky’s films have captivated serious movie audiences all over the world, who see in his work a continuation of the great literary traditions of nineteenth-century Russia. Many critics have tried to interpret his intensely personal vision, but he himself always remained inaccessible.',
        ]);

        Book::create([
            'title' => 'Look Back',
            'author' => 'Tatsuki Fujimoto',
            'description' => 'Fujino is a fourth grader who draws a manga strip for the school newspaper. Her art makes her the star of the class, but one day she’s told that Kyomoto, a student who refuses to come to school, would also like to submit a manga for the paper…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/idjmle.jpg',
        ]);

        Book::create([
            'title' => 'The Stranger',
            'author' => 'Albert Camus',
            'description' => 'Through the story of an ordinary man unwittingly drawn into a senseless murder on an Algerian beach, Camus explored what he termed “the nakedness of man faced with the absurd.”',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/x7nziv.jpg',
        ]);

        Book::create([
            'title' => 'Steve Jobs',
            'author' => 'Walter Isaacson',
            'description' => 'Based on more than forty interviews with Steve Jobs conducted over two years—as well as interviews with more than 100 family members, friends, adversaries, competitors, and colleagues—Walter Isaacson has written a riveting story of the roller-coaster life and searingly intense personality of a creative entrepreneur whose passion for perfection and ferocious drive revolutionized six industries: personal computers, animated movies, music, phones, tablet computing, and digital publishing. Isaacson’s portrait touched millions of readers.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/rg6zyv.jpg',
        ]);

        Book::create([
            'title' => 'Clean Code: A Handbook of Agile Software Craftsmanship',
            'author' => 'Robert C. Martin',
            'description' => 'Even bad code can function. But if code isn’t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn’t have to be that way.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/lzzgu6.jpg',
        ]);

        Book::create([
            'title' => 'The Clean Coder: A Code of Conduct for Professional Programmers',
            'author' => 'Robert C. Martin',
            'description' => 'Programmers who endure and succeed amidst swirling uncertainty and nonstop pressure share a common attribute: They care deeply about the practice of creating software. They treat it as a craft. They are professionals.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/tdzsic.jpg',
        ]);

        Book::create([
            'title' => 'Notes on the Cinematograph',
            'author' => 'Robert Bresson',
            'description' => 'The French film director Robert Bresson was one of the great artists of the twentieth century and among the most radical, original, and radiant stylists of any time. He worked with nonprofessional actors—models, as he called them—and deployed a starkly limited but hypnotic array of sounds and images to produce such classic works as A Man Escaped, Pickpocket, Diary of a Country Priest, and Lancelot of the Lake. From the beginning to the end of his career, Bresson dedicated himself to making movies in which nothing is superfluous and everything is always at stake.',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/p7ceug.jpg',
        ]);

        Book::create([
            'title' => 'Islam Bicara Seni',
            'author' => 'Dr. Yusuf Qardhawi',
            'description' => 'Ada bentuk dosa lain selain perilaku menghalalkan apa yang diharamkan Allah, yakni mengharamkan apa yang dihalalkan oleh-Nya. Kalau yang pertama pelakunya kebanyakan orang awam atau orang alim yang sengaja durhaka, maka yang kedua biasanya menimpa aktivis dakwah dan ahli ibadah yang bersikap ekstrem dalam menjalankan agama. Yang pertama lebih disebabkan oleh kedurhakaan dan keingkaran, sedang yang kedua lebih disebabkan oleh kesalahan dalam memahami sinyal-sinyal syariat agama.',
        ]);

        Book::create([
            'title' => 'Nichijou, 10',
            'author' => 'Keiichi Arawi',
            'description' => 'A game of musical chairs takes a sharp turn. Nano pulls out all the stops in a battle against a fry thief. A bizarre body-swap situation occurs in the Shinonome household. Yuuko displays a talent for naming things. We get a glimpse of Mio’s dream job, as everyone writes a letter to their future selves…',
            'cover' => 'https://res.cloudinary.com/p2k8g2b2/image/upload/cover/jf2p08.jpg',
        ]);
    }
}
