<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function() {
    return response()->json([
        // 'message' => 'Welcome to our API',
        // 'status' => 'OK',
        '🔥' => '🔥',
    ]);
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp', 'RegenerateOtpController')->name('auth.regenerate_otp');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');

    Route::middleware('auth:api')->post('me', function() {
        return response()->json(auth()->user(), 200);
    })->name('auth.me');

    Route::middleware('auth:api')->post('logout', function() {
        return response()->json(['🔥' => '🔥'], 200);
    })->name('auth.logout');
});

Route::get('reviews/latest', 'ReviewController@latest')->name('reviews.latest');
Route::apiResource('reviews', 'ReviewController');

Route::get('books/latest', 'BookController@latest')->name('books.latest');
Route::apiResource('books', 'BookController')->only(['index', 'show']);

Route::get('users/{id}', function($id) {
    return response()->json(App\User::findOrFail($id), 200);
});
