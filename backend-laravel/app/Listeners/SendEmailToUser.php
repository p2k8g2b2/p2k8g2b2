<?php

namespace App\Listeners;

use App\Events\OtpStoredEvent;
use App\Mail\OtpMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpStoredEvent  $event
     * @return void
     */
    public function handle(OtpStoredEvent $event)
    {
        Mail::to($event->otp->user->email)
                ->send(new OtpMail($event->otp, $event->newUserStatus));
    }
}
