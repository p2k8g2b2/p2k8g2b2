<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Success!',
            'data' => $reviews,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            // user_id dipindah ke model Review pada event creating
            // 'user_id' => 'required',
            'book_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $review = Review::create([
            'content' => $request->content,
            // 'user_id' => $request->user_id,
            'book_id' => $request->book_id,
        ]);

        if ($review) {
            return response()->json([
                'success' => true,
                'message' => 'Success!',
                'data' => $review,
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Conflict!',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'Success!',
            'data' => $review,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $review = Review::findOrFail($id);

        if ($review) {
            $user = auth()->user();

            if ($review->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden!',
                ], 403);
            }

            $review->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Success!',
                'data' => $review,
            ], 200);
        }

        // Tidak perlu karena udah pake findOrFail?
        // return response()->json([
        //     'success' => false,
        //     'message' => 'Not found!',
        // ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::findOrFail($id);

        if ($review) {
            $user = auth()->user();

            if ($review->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden!',
                ], 403);
            }

            $review->delete();

            return response()->json([
                'success' => true,
                'message' => 'Success!',
            ], 200);
        }

        // Tidak perlu karena udah pake findOrFail?
        // return response()->json([
        //     'success' => false,
        //     'message' => 'Not found!',
        // ], 404);
    }

    public function latest()
    {
        // dd('OK!');

        $reviews = Review::latest()->take(4)->get();

        return response()->json([
            'message' => 'Success!',
            'reviews' => $reviews,
        ], 200);
    }
}
