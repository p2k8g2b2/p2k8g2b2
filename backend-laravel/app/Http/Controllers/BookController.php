<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $books = Book::latest()->get();
        // $books = Book::paginate(8);
        $books = Book::latest()->paginate(8);

        // return response()->json([
        //     'success' => true,
        //     'message' => 'Success!',
        //     'data' => $books,
        // ], 200);

        // return response()->json([
        //     // 'success' => true,
        //     'message' => 'Success!',
        //     'books' => $books,
        // ], 200);

        return response()->json([
            'message' => 'Success!',
            'books' => $books,
        ], 200);
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);

        // $cover = $book->cover;

        // if ($cover) {
        //     $coverUrl = 'https://res.cloudinary.com/p2k8g2b2/image/upload/v1/' . $cover . '.jpg';

        //     // return response()->json([
        //     //     'success' => true,
        //     //     'message' => 'Success!',
        //     //     'data' => [
        //     //         'book' => $book,
        //     //         'cover' => [
        //     //             'url' => $coverUrl,
        //     //         ],
        //     //     ],
        //     // ], 200);

        //     return response()->json([
        //         // 'success' => true,
        //         'message' => 'Success!',
        //         'book' => [
        //             'id' => $book->id,
        //             'title' => $book->title,
        //             'author' => $book->author,
        //             'description' => $book->description,
        //             'cover' => [
        //                 'url' => $coverUrl,
        //             ],
        //         ],
        //     ], 200);
        // }

        return response()->json([
            'success' => true,
            'message' => 'Success!',
            'book' => $book,
        ], 200);

        // return response()->json([
        //     // 'success' => true,
        //     'message' => 'Success!',
        //     'book' => [
        //         'id' => $book->id,
        //         'title' => $book->title,
        //         'author' => $book->author,
        //         'description' => $book->description,
        //     ],
        // ], 200);
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }

    public function latest()
    {
        // dd('OK!');

        $books = Book::latest()->take(4)->get();

        return response()->json([
            'message' => 'Success!',
            'books' => $books,
        ], 200);
    }
}
