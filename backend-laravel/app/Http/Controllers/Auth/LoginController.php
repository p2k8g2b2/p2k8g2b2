<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        // dd($requestAll);

        $validator = Validator::make($requestAll, [
            'email' => 'required',
            'password' => 'required',
            // 'username' => 'required|unique:users,username',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        // dd($credentials);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                // 'error' => 'Unauthorized',
                'success' => false,
                'message' => 'Invalid Credentials',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Login Successful',
            'data' => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 200);

        // return $this->respondWithToken($token);
    }
}
