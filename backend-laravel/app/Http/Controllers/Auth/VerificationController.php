<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        // dd($requestAll);

        $validator = Validator::make($requestAll, [
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp = Otp::where('otp', $requestAll['otp'])->first();

        if (!$otp) {
            return response()->json([
                'success' => false,
                'message' => 'OTP is not valid',
            ], 400);
        }

        $user = $otp->user;

        if ($otp->valid_until < Carbon::now()) {
            return response()->json([
                'success' => false,
                'message' => 'OTP has expired',
                'errors' => [
                    'otp' => 'OTP has expired'
                ]
            ], 404);
        }

        $user->update([
            'email_verified_at' => Carbon::now()
        ]);

        $otp->delete();

        return response()->json([
            'success' => true,
            'message' => 'Congratulations, your account has been verified',
            'data' => [
                'user' => $user
            ]
        ], 200);

        // dd($user);
    }
}
