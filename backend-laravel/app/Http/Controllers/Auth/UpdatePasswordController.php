<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        // dd($requestAll);

        $validator = Validator::make($requestAll, [
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
            // 'username' => 'required|unique:users,username',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $requestAll['email'])->first();

        if(!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found',
                'errors' => [
                    'email' => 'User not found'
                ]
            ], 404);
        }

        $user->update([
            'password' => Hash::make($requestAll['password'])
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password has been updated',
            'data' => [
                'user' => $user
            ]
        ], 200);
    }
}
