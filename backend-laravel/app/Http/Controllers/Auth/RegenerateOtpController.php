<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use App\User;
use Carbon\Carbon;
use App\Events\OtpStoredEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        // dd($requestAll);

        $validator = Validator::make($requestAll, [
            'email' => 'required'
            // 'username' => 'required|unique:users,username',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $requestAll['email'])->first();

        // dd($user);

        if ($user->otp) {
            $user->otp->delete();
        }

        do {
            $random = mt_rand(100000, 999999);
            $check = Otp::where('otp', $random)->first();

            // dd($check);
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(15);

        $otp = Otp::create([
            'otp' => $random,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        // for debugging email view
        // $newUserStatus = false;

        // return view('mails.otp.mail', compact([
        //     'otp',
        //     'newUserStatus'
        // ]));

        event(new OtpStoredEvent($otp));

        return response()->json([
            'success' => true,
            'message' => 'OTP has been regenerated, please check your email',
            'data' => [
                'user' => $user,
                'otp' => $otp
            ]
        ], 201);
    }
}
