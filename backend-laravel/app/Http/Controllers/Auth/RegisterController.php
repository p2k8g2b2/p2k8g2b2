<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Events\OtpStoredEvent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        // dd($requestAll);

        $validator = Validator::make($requestAll, [
            // 'name' => 'required',
            // 'email' => 'required|email|unique:users',
            'username' => [
                'required',
                // Regex untuk alpha_dash tanpa dash
                'regex:/^[A-Za-z0-9_]+$/',
                'max:30',
                'unique:users,username',
            ],
            'email' => 'required|email|unique:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 400);
        }

        $user = User::create($requestAll);

        do {
            $random = mt_rand(100000, 999999);
            $check = Otp::where('otp', $random)->first();

            // dd($check);
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(15);

        $otp = Otp::create([
            'otp' => $random,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        event(new OtpStoredEvent($otp, true));

        return response()->json([
            'success' => true,
            'message' => 'User Created, please check your email for OTP',
            'data' => [
                'user' => $user,
                'otp' => $otp
            ]
        ], 201);
    }
}
