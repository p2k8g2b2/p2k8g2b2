<?php

namespace App\Mail;

use App\Otp;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OtpMail extends Mailable
{
    use Queueable, SerializesModels;

    public $otp, $newUserStatus;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Otp $otp, $newUserStatus)
    {
        $this->otp = $otp;
        $this->newUserStatus = $newUserStatus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.otp.mail-simple')
                ->subject('OTP @ ' . config('app.name'));
    }
}
