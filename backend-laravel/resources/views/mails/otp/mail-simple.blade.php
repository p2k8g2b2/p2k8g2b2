<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP @ {{config('app.name')}}</title>
</head>
<body>
    <p>Hai, {{'@' . $otp->user->username}}</p>
    {!!$newUserStatus ? '<p>Selamat datang di ' . config('app.name') . '</p>' : ''!!}
    <p>Ini adalah OTP Anda: {{ $otp->otp }}</p>
    <p>OTP ini berlaku sampai {{date('d-m-Y, H.i.s', strtotime($otp->valid_until))}} WIB</p>
    <p><i>Jangan beri tahu siapa pun~</i></p>
</body>
</html>
